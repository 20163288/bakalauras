from typing import List, Optional, Dict, Union, Iterator, Iterable, Any
from collections import Counter
import pandas as pd

def most_common_element(List: List) -> Optional[Any]:
    """
    Returns the most common element in a list.
    
    Parameters:
        List (List): The list of elements.
    
    Returns:
        Optional[Any]: The most common element, or None if the list is empty.
    """
    if List:
        occurence_count = Counter(List)
        return occurence_count.most_common(1)[0][0]
    return None


def structure_classification_results(column_classification_results: Any) -> Dict[str, Any]:
    """
    Structures the classification results for a column.
    
    Parameters:
        column_classification_results (Any): The classification results for a column.
    
    Returns:
        Dict[str, Any]: A dictionary representing the structured classification results.
    """
    col_name = column_classification_results.key

    entity_types = [classification[0].entity_type for classification in column_classification_results.recognizer_results if classification]
    entity_type = most_common_element(entity_types)
    if entity_type:
        classification_scores = [classification[0].score for classification in column_classification_results.recognizer_results if classification]
        avg_classification_score = sum(classification_scores) / len(column_classification_results.recognizer_results)
        if avg_classification_score < 0.4:
            entity_type = None
    return {'column_name': col_name,
            'entity_type': entity_type if entity_type else None,
            'is_pii': True if entity_type else False}


def highlight_pii(s: pd.Series) -> List[str]:
    """
    Highlights PII columns in a DataFrame.
    
    Parameters:
        s (pd.Series): The Series representing a row in the DataFrame.
    
    Returns:
        List[str]: A list of CSS styles to highlight PII columns.
    """
    return ['background-color: green']*len(s) if not s.entity_type else ['background-color: red']*len(s)


def add_df_to_full_report(table_name: str, table_df: pd.DataFrame, report_df: pd.DataFrame) -> pd.DataFrame:
    """
    Adds a DataFrame representing a table to the full report DataFrame.
    
    Parameters:
        table_name (str): The name of the table.
        table_df (pd.DataFrame): The DataFrame representing the table.
        report_df (pd.DataFrame): The full report DataFrame to which the table DataFrame will be added.
    
    Returns:
        pd.DataFrame: The updated full report DataFrame.
    """
    tmp_df = table_df.copy(deep=True)
    tmp_df.insert(0, 'table_name', table_name.split('.')[1])
    tmp_df.insert(0, 'table_schema', table_name.split('.')[0])
    report_df = pd.concat([report_df, tmp_df])
    tmp_df.drop(tmp_df.index, inplace=True)
    return report_df