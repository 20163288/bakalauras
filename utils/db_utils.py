from typing import List, Dict, Any
import streamlit as st
from psycopg2.extensions import connection
from snowflake.connector import SnowflakeConnection
from mysql.connector import MySQLConnection
from pyodbc import Connection

def get_query_results(conn: Any, query: str) -> List[Dict[str, Any]]:
    """
    Executes a query on the given database connection and retrieves the query results.
    
    Parameters:
        conn (Any): The database connection object.
        query (str): The SQL query to execute.
    
    Returns:
        List[Dict[str, Any]]: A list of dictionaries representing the query results. Each dictionary represents a row, with column names as keys and corresponding values.
    """
    with conn.cursor() as cursor:
        cursor.execute(query)
        columns = cursor.description 
        query_results = [{columns[index][0].lower(): column for index, column in enumerate(value)} for value in cursor.fetchall()]
    return query_results


def get_randomized_table_data(db_conn: Any, table: str) -> List[Dict[str, Any]]:
    """
    Retrieves a random set of data from a table.
    
    Parameters:
        db_conn (Any): The database connection object.
        table (str): The name of the table.
    
    Returns:
        List[Dict[str, Any]]: A list of dictionaries representing the retrieved data.
    """
    query = st.session_state['db_config']['randomized_data'].format(table=table, sample_size=st.session_state['sample_size'])
    data = get_query_results(db_conn, query)
    return data


def get_col_information(db_conn: Any, table: str) -> List[Dict[str, Any]]:
    """
    Retrieves column information for a table.
    
    Parameters:
        db_conn (Any): The database connection object.
        table (str): The name of the table.
    
    Returns:
        List[Dict[str, Any]]: A list of dictionaries representing the column information.
    """
    query = st.session_state['db_config']['column_information'].format(table_schema=table.split(".")[0].lower(),
                                                                       table_name=table.split(".")[1].lower())
    data = get_query_results(db_conn, query)
    return data
