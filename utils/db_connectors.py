import psycopg2
import snowflake.connector
import mysql.connector
import pyodbc
from typing import Dict, Any
from psycopg2.extensions import connection
from snowflake.connector import SnowflakeConnection
from mysql.connector import MySQLConnection
from pyodbc import Connection

def connect_postgres(params: Dict[str, Any]) -> connection:
    """
    Establishes a connection with a PostgreSQL database.
    
    Parameters:
        params (Dict[str, Any]): A dictionary containing the connection parameters.
            - database_name (str): The name of the database.
            - database_user (str): The username for the database connection.
            - database_password (str): The password for the database connection.
            - host (str): The host address of the database server.
            - port (str): The port number of the database server.
    
    Returns:
        connection: A psycopg2 connection object.
    """
    return psycopg2.connect(
        dbname=params['database_name'],
        user=params['database_user'],
        password=params['database_password'],
        host=params['host'],
        port=params['port']
    )

def connect_snowflake(params: Dict[str, Any]) -> SnowflakeConnection:
    """
    Establishes a connection with a Snowflake database.
    
    Parameters:
        params (Dict[str, Any]): A dictionary containing the connection parameters.
            - database_user (str): The username for the database connection.
            - database_password (str): The password for the database connection.
            - host (str): The host address of the Snowflake server.
            - database_name (str): The name of the Snowflake database.
            - warehouse (str): The name of the Snowflake warehouse.
            - role (str): The name of the Snowflake role.
    
    Returns:
        SnowflakeConnection: A snowflake.connector connection object.
    """
    return snowflake.connector.connect(
        user=params['database_user'],
        password=params['database_password'],
        account=params['host'],
        database=params['database_name'],
        schema='PUBLIC',
        warehouse=params['warehouse'],
        role=params['role']
    )

def connect_mysql(params: Dict[str, Any]) -> MySQLConnection:
    """
    Establishes a connection with a MySQL database.
    
    Parameters:
        params (Dict[str, Any]): A dictionary containing the connection parameters.
            - host (str): The host address of the MySQL server.
            - database_user (str): The username for the database connection.
            - database_password (str): The password for the database connection.
            - database_name (str): The name of the MySQL database.
    
    Returns:
        MySQLConnection: A mysql.connector connection object.
    """
    return mysql.connector.connect(
        host=params['host'],
        user=params['database_user'],
        password=params['database_password'],
        database=params['database_name']
    )

def connect_mssql(params: Dict[str, Any]) -> Connection:
    """
    Establishes a connection with a Microsoft SQL Server database.
    
    Parameters:
        params (Dict[str, Any]): A dictionary containing the connection parameters.
            - host (str): The host address of the SQL Server.
            - database_user (str): The username for the database connection.
            - database_password (str): The password for the database connection.
            - database_name (str): The name of the SQL Server database.
    
    Returns:
        Connection: A pyodbc connection object.
    """
    return pyodbc.connect(
        'DRIVER={ODBC Driver 17 for SQL Server};'
        f"SERVER={params['host']};"
        f"DATABASE={params['database_name']};"
        f"UID={params['database_user']};"
        f"PWD={params['database_password']};"
    )