import streamlit as st
from utils.db_connectors import connect_mssql, connect_mysql, connect_postgres, connect_snowflake
from streamlit_extras.switch_page_button import switch_page
import os
import json

st.set_page_config(initial_sidebar_state="collapsed", layout="wide")

st.markdown(
    """
<style>
    [data-testid="collapsedControl"] {
        display: none
    }
</style>
""",
    unsafe_allow_html=True,
)

@st.cache_resource
def get_database_connection(database_type,
                            database_name,
                            database_user,
                            database_password,
                            host,
                            port,
                            schema=None,
                            warehouse=None,
                            role=None):
    connect_func = DATABASE_FUNCTIONS.get(database_type.lower())
    if connect_func:
        return connect_func(database_name, database_user, database_password, host, port, schema, warehouse, role)
    else:
        st.error("Invalid database type")
        return None

DATABASE_FUNCTIONS = {
    'postgres': connect_postgres,
    'snowflake': connect_snowflake,
    'mysql': connect_mysql,
    'mssql': connect_mssql
}

@st.cache_resource
def get_database_connection(database_type,
                            params):
    connect_func = DATABASE_FUNCTIONS.get(database_type.lower())
    if connect_func:
        return connect_func(params)
    else:
        st.error("Invalid database type")
        return None


st.title("Database PII Detection Tool")

# Get user input for database connection details
database_type = st.selectbox("Select a database type:", ["Postgres", "Snowflake", "MySQL", "MSSQL"])
st.session_state['db_type'] = database_type.lower()
database_name = st.text_input("Enter database name:", value='postgres')
database_user = st.text_input("Enter database user:", value='postgres')
database_password = st.text_input("Enter database password:", type='password', value="1234")
host = st.text_input("Enter host", value='localhost')
port = None
database_warehouse = None
database_role = None

if database_type in ["Postgres", "MySQL"]:
    port = st.text_input("Enter port number", value='6666')

if database_type == 'Snowflake':
    database_warehouse = st.text_input("Enter database warehouse:")
    database_role = st.text_input("Enter database role:")

# Connect to the database
if st.button("Connect"):
    params = {
        'database_name': database_name,
        'database_user': database_user,
        'database_password': database_password,
        'host': host,
        'port': port,
        'warehouse': database_warehouse,
        'role': database_role
    }
    connection = get_database_connection(database_type,
                                         params)
    st.session_state['db_conn'] = connection
    st.session_state['db_name'] = database_name
    with open(f"{os.getcwd()}/db_configs/{st.session_state['db_type']}.json") as f:
        st.session_state['db_config'] = json.loads(f.read())
    switch_page('available_tables')
