import streamlit as st
import pandas as pd
from presidio_analyzer import BatchAnalyzerEngine
from utils.db_utils import get_query_results, get_randomized_table_data, get_col_information
from utils.utils import most_common_element, structure_classification_results, highlight_pii, add_df_to_full_report
from streamlit_extras.switch_page_button import switch_page
from pandas.core.frame import DataFrame

conn = st.session_state['db_conn']
database_type = st.session_state['db_type']
database_name = st.session_state['db_name']
tables_to_check = st.session_state['tables_to_check']
schemas = {full_table_name.split('.')[0] for full_table_name in st.session_state['tables_to_check']}

analyzer = BatchAnalyzerEngine()

st.title("Classification results")

report_df = pd.DataFrame()

for schema in schemas:
    tables_in_schema = [table for table in tables_to_check if table.startswith(schema + '.')]

    with st.expander(schema, expanded=True):
        for table in tables_in_schema:
            table_data = get_randomized_table_data(conn, table)

            df = pd.DataFrame(table_data).astype(str)

            classification_results = []
            for result in analyzer.analyze_dict(df.to_dict(orient="list"), language="en"):
                classification_results.append(structure_classification_results(result))

            classification_results_df = pd.DataFrame(classification_results)

            col_info = get_col_information(conn, table)
            col_info_df = pd.DataFrame(col_info, dtype=str)

            final_df = col_info_df.merge(classification_results_df, how='left', on='column_name')
            final_df['character_maximum_length'] = final_df['character_maximum_length'].astype(str).str.split('.').str[0]
            final_df['character_maximum_length'].replace({'nan': None}, inplace=True)

            report_df = add_df_to_full_report(table_name=table, table_df=final_df, report_df=report_df)

            st.markdown(f'**{table}**')
            st.dataframe(final_df.style.apply(highlight_pii, axis=1).format(precision=2).hide(axis='index'))

@st.cache_data
def convert_df(df):
    return df.to_csv(index=False).encode('utf-8')

csv = convert_df(report_df)
st.download_button(
"Download report",
csv,
f"classification_report.csv",
"text/csv",
key=f'download-csv'
)

if st.button('Back'):
    st.session_state['tables_to_check'] = []
    switch_page('available_tables')
