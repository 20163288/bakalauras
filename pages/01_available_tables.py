import streamlit as st
import pandas as pd
from streamlit_extras.switch_page_button import switch_page
from utils.db_utils import get_query_results
from typing import Any, List, Dict

st.markdown(
    """
<style>
    [data-testid="collapsedControl"] {
        display: none
    }
</style>
""",
    unsafe_allow_html=True,
)

def get_available_tables(db_conn: Any) -> List[Dict[str, Any]]:
    """
    Retrieves all available tables from the database.
    
    Parameters:
        db_conn (Any): The database connection object.
    
    Returns:
        List[Dict[str, Any]]: A list of dictionaries representing the available tables.
    """
    query = st.session_state['db_config']['available_tables']
    results = get_query_results(db_conn, query)

    if len(results) == 0:
        st.warning("No tables found inside the database.")
    else:
        return results


db_conn = st.session_state['db_conn']
database_type = st.session_state['db_type']
database_name = st.session_state['db_name']

st.header('Tables available in the database:')

db_tables = get_available_tables(db_conn)

st.session_state['tables_to_check'] = st.session_state.get('tables_to_check', [])
schemas = {table_info['table_schema'] for table_info in db_tables}

for schema in schemas:
                
    mark_checkboxes = False

    # Create a container for each schema
    with st.expander(schema, expanded=True):
        # Get all tables in the current schema
        tables_in_schema = [f"{table_info['table_schema']}.{table_info['table_name']}" for table_info in db_tables if table_info['table_schema'] == schema]

        # Create a "select all" button for the schema
        select_all_in_schema_button = st.button(f'Select all tables in {schema}')
        if select_all_in_schema_button:
            mark_checkboxes = True
            st.session_state['tables_to_check'] += tables_in_schema

        # Create checkboxes for each table
        for full_table_name in tables_in_schema:
            checkbox = st.checkbox(full_table_name, key=full_table_name, value=mark_checkboxes)  # use full_table_name as key
            if checkbox and not select_all_in_schema_button:
                st.session_state['tables_to_check'].append(full_table_name)

sample_size = st.slider('Choose sample size in each table for classification', 0, 500, 10, step=10)

if st.button('Begin classification'):
    st.session_state['tables_to_check'] = set(st.session_state['tables_to_check'])
    st.session_state['sample_size'] = sample_size
    switch_page('classify_tables')

if st.button('Back'):
    st.session_state['tables_to_check'] = []
    switch_page('pii_classification_app')


